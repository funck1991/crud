import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormularioCompanyServiceService } from '../_service/formulario-company-service.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit, OnChanges {

  public listadoCompanies: any[];
  constructor(private servicioCompany: FormularioCompanyServiceService, private router: Router) { }

  ngOnInit(): void {    
    this.servicioCompany.list().subscribe(lista => {this.listadoCompanies = lista})  
    console.log(this.listadoCompanies);
  }

  ngOnChanges(){

  }


  solicitarEliminacionCliente(id: string){
    
    this.servicioCompany.delete(id).subscribe(() => {
      this.servicioCompany.list().subscribe(lista => {this.listadoCompanies = lista})//vuelve a listar una vez eliminado
    });
  }

  solicitarEdicionCliente(elemento: any){
    console.log('listado-companies component',elemento);
    this.servicioCompany.companyElementChange(elemento);

    let navigationExtras: NavigationExtras = {
      queryParams: {
          "edit": "true"
      }
    };
    
    this.router.navigate(['/company-form'], navigationExtras);
  }

}



//CompanyListComponent