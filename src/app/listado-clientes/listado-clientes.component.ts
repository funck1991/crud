import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormularioClienteServiceService } from '../_service/formulario-cliente-service.service';

@Component({
  selector: 'app-listado-clientes',
  templateUrl: './listado-clientes.component.html',
  styleUrls: ['./listado-clientes.component.css']
})
export class ListadoClientesComponent implements OnInit, OnChanges {

  public listadoClientes: any[];
  existingEvent:any;
  constructor(private servicioCliente: FormularioClienteServiceService, private router: Router) { }

  ngOnInit(): void {
    //this.servicioCliente.list();
    
    this.servicioCliente.list().subscribe(lista => {this.listadoClientes = lista})  
    console.log(this.listadoClientes);
  }

  ngOnChanges(){

  }
  deletesVariable: any;

  solicitarEliminacionCliente(id: string){
    
    this.servicioCliente.delete(id).subscribe(() => {
      this.servicioCliente.list().subscribe(lista => {this.listadoClientes = lista})//vuelve a listar una vez eliminado
    });
  }

  solicitarEdicionCliente(elemento: any){
    console.log('listado-clientes component',elemento);
    this.servicioCliente.contactoElementChange(elemento);

    let navigationExtras: NavigationExtras = {
      queryParams: {
          "edit": "true"
      }
    };
    
    this.router.navigate(['/formularioCliente'], navigationExtras);
  }

}
