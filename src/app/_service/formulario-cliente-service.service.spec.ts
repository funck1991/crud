import { TestBed } from '@angular/core/testing';

import { FormularioClienteServiceService } from './formulario-cliente-service.service';

describe('FormularioClienteServiceService', () => {
  let service: FormularioClienteServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormularioClienteServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
