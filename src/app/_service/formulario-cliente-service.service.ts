import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable ,  BehaviorSubject } from 'rxjs';
import { Body } from '@angular/http/src/body';
import { NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormularioClienteServiceService {

  private contactos = new BehaviorSubject<Array<any>>([{}]);
  contactosList= this.contactos.asObservable();
  private contacto = new BehaviorSubject<any>({});
  contactoElement= this.contacto.asObservable();
  
  constructor(private http: Http) {
}

   create(params: any) {
    return this.http.post('http://localhost:3000/users', params).pipe(
        map(data => data.json()));
}

    contactosListChange(list: Array<any>){
      this.contactos.next(list);
    }
    contactoElementChange(list: any){
      this.contacto.next(list);
    }



    list(){
      return this.http.get('http://localhost:3000/users').pipe(
        map(data => data.json()));

    }

    

    delete(id: string)
    {return this.http.delete('http://localhost:3000/users',{body: {'_id':id}

      }).pipe(
        map(data => data.json()));
    }

    edit(params: any)
    {return this.http.put('http://localhost:3000/users',params
      ).pipe(map(data => data.json()));

    }

    

}
