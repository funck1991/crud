import { TestBed } from '@angular/core/testing';

import { FormularioCompanyServiceService } from './formulario-company-service.service';

describe('FormularioCompanyServiceService', () => {
  let service: FormularioCompanyServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormularioCompanyServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
