import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import {map} from 'rxjs/operators';
import { Observable ,  BehaviorSubject } from 'rxjs';
import { Body } from '@angular/http/src/body';
import { NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormularioCompanyServiceService {

  private companies = new BehaviorSubject<Array<any>>([{}]);
  companiesList= this.companies.asObservable();
  private company = new BehaviorSubject<any>({});
  companyElement= this.company.asObservable();
  
  constructor(private http: Http) {
}

   create(params: any) {
    return this.http.post('http://localhost:3000/company', params).pipe(
        map(data => data.json()));
}

    companiesListChange(list: Array<any>){
      this.companies.next(list);
    }
    companyElementChange(list: any){
      this.company.next(list);
    }



    list(){
      return this.http.get('http://localhost:3000/company').pipe(
        map(data => data.json()));

    }

    

    delete(id: string)
    {return this.http.delete('http://localhost:3000/company',{body: {'_id':id}

      }).pipe(
        map(data => data.json()));
    }

    edit(params: any)
    {return this.http.put('http://localhost:3000/company',params
      ).pipe(map(data => data.json()));

    }

    

}
