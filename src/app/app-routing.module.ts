import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyFormComponent } from './company-form/company-form.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { FormularioClienteComponent } from './formulario-cliente/formulario-cliente.component';
import { LandingCompanyComponent } from './landing-company/landing-company.component';
import { LandingComponent } from './landing/landing.component';
import { ListadoClientesComponent } from './listado-clientes/listado-clientes.component';

const routes: Routes = [
  //company
  {
    path: '',
    component: LandingCompanyComponent
  },
  {
    path: 'landing-company',
    component: LandingCompanyComponent
  },
  {
    path: 'company-form',
    component: CompanyFormComponent
  },
  {
    path: 'company-list',
    component: CompanyListComponent
  },
  //contact
  {
    path: '',
    component: LandingComponent
  },
  {
    path: 'landing',
    component: LandingComponent
  },
  {
    path: 'formularioCliente',
    component: FormularioClienteComponent
  },
  {
    path: 'listadoClientes',
    component: ListadoClientesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
